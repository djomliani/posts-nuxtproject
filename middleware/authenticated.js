export default function ({ store, redirect }) {
    if (!store.state.users.isAuthenticated) {
        return redirect('/auth/login')
    }
}
