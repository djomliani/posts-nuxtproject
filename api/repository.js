export default $axios => resource => ({
    getElements (postId) {
        if (postId) {
            return $axios.$get(`${resource}?postId=${postId}`)
        } else {
            return $axios.$get(`${resource}`)
        }
    },

    getElement (id) {
        return $axios.$get(`${resource}/${id}`)
    },

    create (payload) {
        return $axios.$post(`${resource}`, payload)
    },

    update (payload) {
        return $axios.$put(`${resource}/${payload.id}`, payload)
    },

    delete (id) {
        return $axios.$delete(`${resource}/${id}`)
    }
})
