import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _7c69b5fe = () => interopDefault(import('..\\pages\\userProfile.vue' /* webpackChunkName: "pages_userProfile" */))
const _b58dcd38 = () => interopDefault(import('..\\pages\\auth\\login.vue' /* webpackChunkName: "pages_auth_login" */))
const _23fa5610 = () => interopDefault(import('..\\pages\\auth\\register.vue' /* webpackChunkName: "pages_auth_register" */))
const _9424ec18 = () => interopDefault(import('..\\pages\\posts\\_id.vue' /* webpackChunkName: "pages_posts__id" */))
const _1823a696 = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages_index" */))

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/userProfile",
    component: _7c69b5fe,
    name: "userProfile"
  }, {
    path: "/auth/login",
    component: _b58dcd38,
    name: "auth-login"
  }, {
    path: "/auth/register",
    component: _23fa5610,
    name: "auth-register"
  }, {
    path: "/posts/:id?",
    component: _9424ec18,
    name: "posts-id"
  }, {
    path: "/",
    component: _1823a696,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
