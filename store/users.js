export const state = () => ({
    users: [],
    user: {},
    isAuthenticated: false
})

export const mutations = {
    SET_USERS (state, users) {
        state.users = users
    },

    LOGIN_USER (state, user) {
        state.user = user
        state.isAuthenticated = true
    },

    LOGOUT_USER (state) {
        state.user = {}
        state.isAuthenticated = false
    }
}

export const actions = {
    async getUsers ({commit}) {
        let users = await this.$usersRepository.getElements()

        commit('SET_USERS', users)
    },

    login ({state, commit}, form) {
        let user = state.users.find(user => {
            return user.username === form.username
        })

        if (user && form.password === 'password') {
            commit('LOGIN_USER', user)
        } else {
            alert('Username or password are not valid!')
        }
    },

    logout ({commit}) {
        commit('LOGOUT_USER')
    }
}
