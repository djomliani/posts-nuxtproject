export const state = () => ({
    posts: [],
    post: {},
    page: 1,
    perPage: 10,
    pages: []
})

export const mutations = {
    SET_POSTS (state, posts) {
        state.posts = posts
    },

    SET_POST (state, post) {
        state.post = post
    },

    SELECT_PAGE (state, page) {
        state.page = page
    },

    SET_PAGES (state, pages) {
        state.pages = pages
    },

    PAGINATE_POSTS (state, payload) {
        state.posts = state.posts.slice(payload.from, payload.to)
    }
}

export const actions = {
    async getPosts ({commit, dispatch}, page) {
        let posts = await this.$postRepository.getElements()
        
        if (page) {
            dispatch('selectedPage', page)
        }
        commit('SET_POSTS', posts)
        dispatch('setPages')
        dispatch('paginatePosts')
    },

    async getPost ({commit}, postId) {
        let post = await this.$postRepository.getElement(postId)

        commit('SET_POST', post)
    },

    selectedPage ({commit}, page) {
        commit('SELECT_PAGE', page)
    },

    setPages ({state, commit}) {
        let numberOfPages = Math.ceil(state.posts.length / state.perPage)
        let pages = []
        
        for (let i=1; i<=numberOfPages; i++) {
            pages.push(i)
        }
        
        commit('SET_PAGES', pages)
    },

    paginatePosts ({state, commit}) {
        let page = state.page
        let perPage = state.perPage
        let from = (page * perPage) - perPage
        let to = page * perPage

        commit('PAGINATE_POSTS', {from, to})
    }
}
