export const state = () => ({
    comments: []
})

export const mutations = {
    SET_COMMENTS (state, comments) {
        state.comments = comments
    }
}

export const actions = {
    async getComments ({commit}, postId) {
        let comments = await this.$commentRepository.getElements(postId)
        
        commit('SET_COMMENTS', comments)
    }
}
